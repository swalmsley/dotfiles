## Dependencies ##
- zsh
- prezto : config for zsh
- powerlevel9k : prompt config for prezto for zsh
- nerd-fonts-complete : fonts used by your terminal emulator for powerlevel9k for prezto for zsh (I use Saucecode Pro)
- colorls : aliased as ls (uses nerd-fonts-complete)

## notes ##
- in .zshrc : you must set all you powerline shizz BEFORE sourcing prezto
- prezto aliases ls AFTER your .zshrc so change your ls alias there (.zprezto/modules/utility/init.zsh) and comment out 'alias ls="${aliases[ls]:-ls} --color=auto;"'
- make sure to add ruby gems to path in your .profile (PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin")